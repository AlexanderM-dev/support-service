import { Router } from 'express';

import { getProductList, getTopicList, getIssue, changeTopicList, changeIssue, changeProductList } from '../controllers/wiki.controller';

export const wikiRouter = Router();

wikiRouter.get('/products', getProductList);
wikiRouter.get('/topics/:productid', getTopicList);
wikiRouter.get('/issues/:productid/:issueId', getIssue);
wikiRouter.put('/products/change/', changeProductList);
wikiRouter.put('/topics/change/:productid', changeTopicList);
wikiRouter.put('/issues/change/:productid/:issueId', changeIssue);
