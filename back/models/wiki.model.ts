export enum RecordStatusEnum {
  added = 'added',
  changed = 'changed',
  deleted = 'deleted'
}

export interface IProductFromServer {
  id: string;
  name: string;
  urlname: string;
  logo: string;
  topics: number;
  issues: number;
}

export interface IProduct {
  id: string;
  name: string;
  urlname: string;
  logo: string;
  topics: number;
  issues: number;
  status?: RecordStatusEnum | undefined;
}

export interface ITopicFromServer {
  topicIndex: number;
  id: string;
  productId: string;
  title: string;
  issueId: string;
  issueTitle: string;
  issueUrlTitle?: string;
  issueIndex: number;
}

export interface IProductTopics {
  topics: ITopic[];
}

export interface ITopic {
  index?: number;
  id: string;
  title: string;
  productId: string;
  issues: IProductIssueLight[];
  status?: RecordStatusEnum;
}

export interface IProductIssueLight {
  index?: number;
  id: string;
  title: string;
  urlTitle?: string;
  status?: RecordStatusEnum;
}

export interface IProductIssueFromServer {
  id: string;
  title: string;
  urlTitle?: string;
  htmlBlocks: string;
  attachments?: any[];
}

export interface IProductIssue {
  issueId: string;
  title: string;
  urlTitle?: string;
  html: IParsedQuillHtml[];
  attachments?: any[];
}

export interface IParsedQuillHtml {
  id: string;
  title: string;
  level: number;
  content: string;
}