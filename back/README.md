# Wiki Constructor Back-end node.js server

Wiki Constructor Back-end node.js server - is a Back-end part of [Wiki Constructor](https://gitlab.com/AlexanderM-dev/support-service)

[Front-end](https://gitlab.com/AlexanderM-dev/support-service/-/tree/master/front) part

## Install

To run this app you need:
1) Install [PostgeSQL](https://www.postgresql.org/download/) database
2) Add a ".env" file with this structure to the APP root:

+ PORT = port for server
+ DB_USER = "SQL-DB-username"
+ DB_PASSWORD = "SQL-DB-password"
+ DB_DB = "SQL-DB-name"
+ DB_HOST = "SQL-DB-host"
+ DB_PORT = SQL-DB-port

3) Run in console `npm i`

## Run

Run in console `npm start`