import { Request, Response } from 'express'

import { WikiDB } from '../classes/wiki.class'
import { IProduct, IProductFromServer, IProductIssue, IProductIssueFromServer, IProductTopics, ITopic, ITopicFromServer } from '../models/wiki.model';


/** Получение списка Products и сортировка по алфавиту */
export async function getProductList(req: Request, res: Response) {
    try {
        const allProducts: IProductFromServer[] | undefined = await WikiDB.getProducts();
        if (allProducts) {
            allProducts.sort(function(a, b) {
                const nameA = a.name.toLowerCase();
                const nameB = b.name.toLowerCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
            res.status(200).json(allProducts)
        } else {
            console.log('Продуктов в базе данных нет');
            res.status(200).json([]);
        }
    } catch (error) {
        console.error(error.message)
        res.status(500).json({
            message: 'Server error. getProductList'
        })
    }
}

/** Получение списка Topics и относящихся к ним Issues без контента, формирование необхожимого массива объектов */
export async function getTopicList(req: Request, res: Response) {
    try {
        const allTopics: ITopicFromServer[] | undefined = await WikiDB.getTopics(req.params.productid);
        const productTopics: IProductTopics = {
            topics: []
        };
        if (allTopics && allTopics.length) {
            const topicsIds: string[] = [];
            for (const topic of allTopics) {
                if (topicsIds.includes(topic.id)) {
                    let topicToIssueAdd: ITopic | undefined = productTopics.topics.find(item => item.id === topic.id)
                    if (topicToIssueAdd && topicToIssueAdd.issues.length) {
                        topicToIssueAdd.issues.push({
                            index: topic.issueIndex,
                            id: topic.issueId,
                            title: topic.issueTitle,
                            urlTitle: topic.issueUrlTitle
                        })
                    }
                } else {
                    topicsIds.push(topic.id);
                    if (topic.issueId && topic.issueIndex && topic.issueTitle) {
                        productTopics.topics.push({
                            index: topic.topicIndex,
                            id: topic.id,
                            title: topic.title,
                            productId: topic.productId,
                            issues: [
                                {
                                    index: topic.issueIndex,
                                    id: topic.issueId,
                                    title: topic.issueTitle,
                                    urlTitle: topic.issueUrlTitle,
                                }
                            ]
                        })
                    } else {
                        productTopics.topics.push({
                            index: topic.topicIndex,
                            id: topic.id,
                            title: topic.title,
                            productId: topic.productId,
                            issues: []
                        })
                    }
                }
            }
            productTopics.topics.sort(function(a, b) {
                return a.index! - b.index!
            })
            for (const topic of productTopics.topics) {
                topic.issues.sort(function(a, b) {
                    return a.index! - b.index!
                })
            }
            productTopics.topics = productTopics.topics.map(topic => {
                return {
                    id: topic.id,
                    title: topic.title,
                    productId: topic.productId,
                    issues: topic.issues.map(issue => {
                        return {
                            id: issue.id,
                            title: issue.title,
                            urlTitle: issue.urlTitle,
                        }
                    })
                }
            })
            res.status(200).json(productTopics)
        } else {
            console.log('Топиков в базе данных нет');
            res.status(200).json(productTopics)
        }
    } catch (error) {
        console.error(error.message)
        res.status(500).json({
            message: 'Server error. getTopicList'
        })
    }
}

/** Получение Issue с контентом */
export async function getIssue(req: Request, res: Response) {
    try {
        const issue: IProductIssueFromServer[] | undefined = await WikiDB.getIssue(req.params.productid, req.params.issueId);
        if (issue && issue.length) {
            res.status(200).json(issue[0])
        }
    } catch (error) {
        console.error(error.message)
        res.status(500).json({
            message: 'Server error. getIssueList'
        })
    }
}

/** Внесение изменений в Products */
export async function changeProductList(req: Request, res: Response) {
    try {
        const incProducts: IProduct[] = req.body
        for (const product of incProducts) {
            if (product.status === 'added') {
                await WikiDB.addNewProduct(product);
            }
            if (product.status === 'deleted') {
                await WikiDB.deleteProduct(product.id);
            }
            if (product.status === 'changed') {
                await WikiDB.updateProduct(product);
            }
        }

        getProductList(req, res);

    } catch (error) {
        if (error.productUpdateError) {
            console.error(error.productUpdateError);
            res.status(400).json({
                message: error.productUpdateError
            })
        } else if (error.productAddError) {
            console.error(error.productAddError);
            res.status(400).json({
                message: error.productAddError
            })
        } else if (error.productDeleteError) {
            console.error(error.productDeleteError);
            res.status(400).json({
                message: error.productDeleteError
            })
        } else {
            console.error(error.message)
            res.status(500).json({
                message: 'Server error. changeProductList'
            })
        }
    }
}

/** Внесение изменений в Topics и относящихся к ним Issues без контента */
export async function changeTopicList(req: Request, res: Response) {
    try {
        const incProductTopics: IProductTopics = req.body
        const productTopicsWithIndex = incProductTopics.topics.map((topic, topicIndex) => {
            return {
                index: topicIndex + 1,
                id: topic.id,
                title: topic.title,
                status: topic.status,
                productId: topic.productId,
                issues: topic.issues.map((issue, issueIndex) => {
                    return {
                        index: issueIndex + 1,
                        id: issue.id,
                        title: issue.title,
                        urlTitle: issue.urlTitle,
                        status: issue.status
                    }
                })
            }
        })

        for (const topic of productTopicsWithIndex) {
            if (topic.status === 'added') {
                await WikiDB.addNewTopic(topic, req.params.productid);
                if (topic.issues.length) {
                    for (const issue of topic.issues) {
                        await WikiDB.addNewIssue(issue, topic);
                    }
                }
            }
            if (topic.status === 'deleted') {
                await WikiDB.deleteTopic(topic.id);
            }
            if (topic.status === 'changed') {
                await WikiDB.updateTopic(topic);
                if (topic.issues.length) {
                    for (const issue of topic.issues) {
                        if (issue.status === 'added') {
                            await WikiDB.addNewIssue(issue, topic);
                        }
                        if (issue.status === 'deleted') {
                            await WikiDB.deleteIssue(issue);
                        }
                        if (issue.status === 'changed') {
                            await WikiDB.updateIssue(issue);
                        }
                    }
                }
            }
        }

        getTopicList(req, res);

    } catch (error) {
        if (error.topicUpdateError) {
            console.error(error.topicUpdateError);
            res.status(400).json({
                message: error.topicUpdateError
            })
        } else if (error.issueUpdateError) {
            console.error(error.issueUpdateError);
            res.status(400).json({
                message: error.issueUpdateError
            })
        } else if (error.issueAddError) {
            console.error(error.issueAddError);
            res.status(400).json({
                message: error.issueAddError
            })
        } else if (error.topicAddError) {
            console.error(error.topicAddError);
            res.status(400).json({
                message: error.topicAddError
            })
        } else if (error.topicDeleteError) {
            console.error(error.topicDeleteError);
            res.status(400).json({
                message: error.topicDeleteError
            })
        } else if (error.issueDeleteError) {
            console.error(error.issueDeleteError);
            res.status(400).json({
                message: error.issueDeleteError
            })
        } else {
            console.error(error.message)
            res.status(500).json({
                message: 'Server error. changeTopicList'
            })
        }
    }
}

/** Внесение изменений в Issue с контентом */
export async function changeIssue(req: Request, res: Response) {
    try {
        const incIssue: IProductIssue = req.body
        await WikiDB.updateIssueWIthContent(incIssue);

        getIssue(req, res);

    } catch (error) {
        console.log(error);
        if (error.issueUpdateError) {
            console.error(error.issueUpdateError);
            res.status(400).json({
                message: error.issueUpdateError
            })
        } else {
            console.error(error.message)
            res.status(500).json({
                message: 'Server error. changeIssue'
            })
        }

    }
}
