import express from 'express';
import * as packageJSON from './package.json';
// Db
import { Client, QueryResult } from 'pg';
// Third party
import cors from 'cors';
import morgan from 'morgan';
import dotenv from 'dotenv';
// local
import { wikiRouter } from './routes/wiki.router';
import { DataBase } from './classes/db.class'


const appVersion: string = packageJSON.version;

dotenv.config();

export const client = new Client({
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DB,
    host: process.env.DB_HOST,
    port: +(process.env.DB_PORT || 5432)
})

const app = express();

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json({limit: '50mb'}));
app.use(cors());

// Routes
app.use(`/api/v${appVersion}/wiki`, wikiRouter);

async function start() {
    try {
        // DB
        await client.connect();
        const findTables: QueryResult = await client.query(`SELECT table_name FROM information_schema.tables WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
                                                            AND table_schema IN('public', 'myschema') AND table_name = 'db_version';`)
        if (findTables.rows.length) {
            const findTableDbVersion: QueryResult = await client.query('SELECT * FROM db_version ORDER BY id DESC LIMIT 1');
            if (findTableDbVersion.rows.length) {
                const dbVersion = await findTableDbVersion.rows[0].version;
                // сравниваем версии приложения и БД
                if (+dbVersion.slice(0, dbVersion.indexOf('.')) === +appVersion.slice(0, appVersion.indexOf('.'))) {
                    console.log('База подключена');
                } else {
                    console.log(`Версия приложения (${appVersion}) и базы данных (${dbVersion}) отличается. Приложение не запущено`);
                    await client.end();
                    return;
                }
            } else {
                console.log('Таблица версии базы данных не обнаружена. Приложение не запущено');
                await client.end();
                return;
            }
        } else {
            await DataBase.create();
            console.log('База создана и подключена');
        };
        const port = process.env.PORT || 3000;
        app.listen(port, () => console.log(`Server has been started on port ${port}`));
    } catch (error) {
        console.log(error)
    }
};

start();