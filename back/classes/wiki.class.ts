import { QueryResult } from 'pg';

import { client } from '../index'
import { IProduct, IProductFromServer, IProductIssue, IProductIssueFromServer, IProductIssueLight, ITopic, ITopicFromServer } from '../models/wiki.model';

export class WikiDB {

    constructor() {}

    static async getProducts(): Promise<IProductFromServer[] | undefined> {
        const qr: QueryResult = await client.query('SELECT id, name, logo, url_name AS urlName FROM products')
        if (qr.rows.length) {
            for (const product of qr.rows) {
                const qr2: QueryResult = await client.query('SELECT count(*) FROM topics WHERE product_id = $1', [product.id]);
                if (qr2.rows.length) {
                    product.topics = +qr2.rows[0].count;
                }
                const qr3: QueryResult = await client.query('SELECT count(*) FROM issues WHERE product_id = $1', [product.id]);
                if (qr3.rows.length) {
                    product.issues = +qr3.rows[0].count;
                }
            }
            return qr.rows
        } else return undefined
    }

    static async getTopics(productid: string): Promise<ITopicFromServer[] | undefined> {
        const qr: QueryResult = await client.query('SELECT topics.index AS "topicIndex", topics.id, topics.product_id AS "productId", topics.title, issues.id AS "issueId", issues.title AS "issueTitle", issues.url_title AS "issueUrlTitle", issues.index AS "issueIndex" FROM topics LEFT JOIN issues ON issues.topic_id = topics.id WHERE topics.product_id = $1', [productid])
        if (qr.rows.length) {
            return qr.rows
        } else return undefined
    }

    static async getIssue(productid: string, issueId: string): Promise<IProductIssueFromServer[] | undefined> {
        const qr: QueryResult = await client.query('SELECT issues.id AS "issueId", issues.title, issues.url_title AS "urlTitle", html_blocks AS "html" FROM topics LEFT JOIN issues ON issues.topic_id = topics.id WHERE topics.product_id = $1 AND issues.id = $2', [productid, issueId]);
        if (qr.rows.length) {
            return qr.rows
        } else return undefined
    }

    static async addNewProduct(product: IProduct): Promise<void> {
        try {
            if (product.logo) {
                await client.query('INSERT INTO products(id, name, url_name, logo) VALUES ($1, $2, $3, $4)', [product.id, product.name, product.urlname, product.logo]);
            } else {
                await client.query('INSERT INTO products(id, name, url_name) VALUES ($1, $2, $3)', [product.id, product.name, product.urlname]);
            }
        } catch (error) {
            throw { error, productAddError: 'Product add error' }
        }
    }

    static async addNewTopic(topic: ITopic, productId: string): Promise<void> {
        try {
            await client.query('INSERT INTO topics(id, product_id, title, index) VALUES ($1, $2, $3, $4)', [topic.id, productId, topic.title, topic.index]);
        } catch (error) {
            throw { error, topicAddError: 'Topic add error' }
        }
    }

    static async addNewIssue(issue: IProductIssueLight, topic: ITopic): Promise<void> {
        try {
            if (issue.urlTitle) {
                await client.query('INSERT INTO issues(id, topic_id, title, url_title, index, product_id) VALUES ($1, $2, $3, $4, $5, $6)', [issue.id, topic.id, issue.title, issue.urlTitle, issue.index, topic.productId]);
            } else {
                await client.query('INSERT INTO issues(id, topic_id, title, index, product_id) VALUES ($1, $2, $3, $4, $5)', [issue.id, topic.id, issue.title, issue.index, topic.productId]);
            }
        } catch (error) {
            throw { error, issueAddError: 'Issue add error' }
        }
    }

    static async deleteProduct(productId: string): Promise<void> {
        try {
            await client.query('DELETE FROM products WHERE id=$1', [productId]);
        } catch (error) {
            throw { error, productDeleteError: 'Product delete error' }
        }
    }

    static async deleteTopic(topicId: string): Promise<void> {
        try {
            await client.query('DELETE FROM topics WHERE id=$1', [topicId]);
        } catch (error) {
            throw { error, topicDeleteError: 'Topic delete error' }
        }
    }

    static async deleteIssue(issue: IProductIssueLight): Promise<void> {
        try {
            await client.query('DELETE FROM issues WHERE id=$1', [issue.id]);
        } catch (error) {
            throw { error, issueDeleteError: 'Issue delete error' }
        }
    }

    static async updateProduct(product: IProduct): Promise<void> {
        try {
            await client.query('UPDATE products SET (name, url_name, logo) = ($1, $2, $3) WHERE id=$4', [product.name, product.urlname, product.logo, product.id]);
        } catch (error) {
            throw { error, productUpdateError: 'Product update error' }
        }
    }

    static async updateTopic(topic: ITopic): Promise<void> {
        try {
            await client.query('UPDATE topics SET (title, index) = ($1, $2) WHERE id=$3', [topic.title, topic.index, topic.id]);
        } catch (error) {
            throw { error, topicUpdateError: 'Topic update error' }
        }
    }

    static async updateIssue(issue: IProductIssueLight): Promise<void> {
        try {
            if (issue.urlTitle) {
                await client.query('UPDATE issues SET (title, url_title, index) = ($1, $2, $3) WHERE id=$4', [issue.title, issue.urlTitle, issue.index, issue.id]);
            } else {
                await client.query('UPDATE issues SET (title, index) = ($1, $2) WHERE id=$3', [issue.title, issue.index, issue.id]);
            }
        } catch (error) {
            throw { error, issueUpdateError: 'Issue update error' }
        }
    }

    static async updateIssueWIthContent(issue: IProductIssue): Promise<void> {
        try {
            if (issue.urlTitle) {
                await client.query('UPDATE issues SET (title, url_title, html_blocks) = ($1, $2, $3) WHERE id=$4', [issue.title, issue.urlTitle, JSON.stringify(issue.html), issue.issueId]);
            } else {
                await client.query('UPDATE issues SET (title, html_blocks, url_title) = ($1, $2, null) WHERE id=$3', [issue.title, JSON.stringify(issue.html), issue.issueId]);
            }
        } catch (error) {
            throw { error, issueUpdateError: 'Issue update error' }
        }
    }

}