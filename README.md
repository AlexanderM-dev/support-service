# Wiki Constructor

Wiki Constructor - is a WEB application that allows you to quickly and very easily create and edit documentation with dynamic navigation through your products, topics and paragraphs

## Video overview

[Wiki Constructor - Overview](https://www.youtube.com/watch?v=vzr3KYzT7JU)

## Install

[Back-end](https://gitlab.com/AlexanderM-dev/support-service/-/tree/master/back)

[Front-end](https://gitlab.com/AlexanderM-dev/support-service/-/tree/master/front)

## Used frameworks and libraries

Front-end part developed using:
+ Angular
+ Angular Material
+ Quill editor
+ Highlight JS

Back-end part developed using:
+ Node.js
+ Express
+ pg (node-postgres)

Data base:
+ PostgreSQL