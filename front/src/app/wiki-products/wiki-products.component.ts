import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
// Local
import { IProduct, RecordStatusEnum, WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-wiki-products',
  templateUrl: './wiki-products.component.html',
  styleUrls: ['./wiki-products.component.scss']
})
export class WikiProductsComponent implements OnInit, OnDestroy {

  RecordStatusEnum = RecordStatusEnum;

  loading$: BehaviorSubject<boolean>;
  products: IProduct[] = [];
  searchString = '';
  edit = false;
  productsShot?: string;

  constructor(
    private service: WikiService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.loading$ = service.loading$;
  }

  ngOnInit(): void {
    this.getProducts();
    this.service.currentProductTitle$.next('');
    this.service.appInitialized$.next(true);
  }

  ngOnDestroy(): void {
    this.service.topicsEdit$.next(false);
  }

  /** Получение Products */
  async getProducts(): Promise<void> {
    try {
      this.products = await this.service.getProductList(true);
    } catch (error) {
      console.error('getProducts: ', error);
    }
  }

  /** Навигация в Topics продукта */
  openTopic(product: IProduct): void {
    if (!this.edit) {
      this.router.navigate([`wiki/${product.urlname}`]);
    }
  }

  /** Тогл редактирования Products */
  toggleEditProducts(): void {
    if (!this.edit) {
      this.productsShot = JSON.stringify(this.products);
      this.edit = true;
      this.service.topicsEdit$.next(true);
      this.products.push({
        id: uuidv4(),
        name: '',
        urlname: '',
        logo: '',
        issues: 0,
        topics: 0,
        addForm: true,
        status: RecordStatusEnum.added
      });
    } else {
      if (this.productsShot) {
        this.products = JSON.parse(this.productsShot);
        this.service.products$.next(this.products);
        this.edit = false;
        this.service.topicsEdit$.next(false);
      }
    }
  }

  /** Сохранение изменения редактирования (отправка на сервер изменений и получение в ответ измененного массива Products) */
  async save(): Promise<void> {
    this.products.pop();
    let productsHasChanges = false;
    for (const product of this.products) {
      if (product.status) {
        productsHasChanges = true;
        break;
      }
    }
    if (productsHasChanges) {
      const prod = await this.service.saveChangedProducts(this.products);
      if (prod) {
        this.products = prod;
      } else {
        this.getProducts();
      }
    }
    this.edit = false;
    this.service.topicsEdit$.next(false);
  }

  /** Вызов mat-dialog с редактированием продукта */
  openDialog(product: IProduct): void {
    this.dialog.open(DialogContentComponent, {
      data: {
        product,
        products: this.products
      }
    });
  }

}

@Component({
  selector: 'app-dialog-content',
  templateUrl: './dialog-content.component.html',
  styleUrls: ['./dialog-content.component.scss']
})
export class DialogContentComponent {

  form: FormGroup;
  allowedSymbols = 'abcdefghijklmnopqrstuvwxyz0123456789-_';
  valueChaged = false;
  settingLogo = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { product: IProduct, products: IProduct[] }
  ) {
    this.form = new FormGroup({
      name: new FormControl(this.data.product.name, [
        Validators.required,
        DialogContentComponent.validateWhiteSpaces
      ]),
      urlName: new FormControl(this.data.product.urlname, [
        Validators.required,
        DialogContentComponent.uniqueUrlName(this.data.products, this.data.product)
      ]),
      logo: new FormControl(this.data.product.logo)
    });
  }

  /** Валидатор пустых пробелов */
  static validateWhiteSpaces(control: FormControl): { [key: string]: boolean } | null {
    if (!control.value.trim()) {
      return {
        whitespaces: true
      };
    }
    return null;
  }

  /** Валидатор уникальности url-name */
  static uniqueUrlName(products: IProduct[], product: IProduct): ValidatorFn {
    return (control: AbstractControl) => {
      for (const prod of products) {
        if (prod.urlname === control.value && prod !== product) {
          return {
            notUnique: true
          };
        }
      }
      return null;
    };
  }

  /** Приведение к валидному url-name */
  toUrlValidName(event: string): void {
    const str = event.replace(/ /g, '-').toLowerCase();
    let result = '';
    for (const symb of str) {
      if (this.allowedSymbols.includes(symb)) {
        result = result + symb;
      }
    }
    const urlTitleControl = this.form.get('urlName');
    if (urlTitleControl) {
      if (result !== urlTitleControl.value) {
        this.form.patchValue({
          urlName: result
        });
      }
    }
  }

  /** Тогл инпута логотипа продукта */
  setLogo(): void {
    this.settingLogo = !this.settingLogo;
  }

  /** Добавление нового product */
  addProduct(): void {
    this.data.products.pop();
    this.data.products.push({
      id: this.data.product.id,
      name: this.form.get('name')?.value,
      urlname: this.form.get('urlName')?.value,
      logo: this.form.get('logo')?.value,
      issues: this.data.product.issues,
      topics: this.data.product.topics,
      status: this.data.product.status
    });
    this.data.products.push({
      id: uuidv4(),
      name: '',
      urlname: '',
      logo: '',
      issues: 0,
      topics: 0,
      addForm: true,
      status: RecordStatusEnum.added
    });
  }

  /** Удаление Product */
  deleteProduct(): void {
    if (this.data.product.status !== RecordStatusEnum.added) {
      this.data.product.status = RecordStatusEnum.deleted;
    }
    if (this.data.product.status === RecordStatusEnum.added) {
      const index = this.data.products.indexOf(this.data.product);
      this.data.products.splice(index, 1);
    }
  }

  /** Установка статуста Product - changed */
  changeProductStatus(): void {
    this.valueChaged = true;
  }

  /** Подтверждение внесенных изменений */
  submitChanges(): void {
    if (this.valueChaged) {
      this.data.product.name = this.form.get('name')?.value;
      this.data.product.urlname = this.form.get('urlName')?.value;
      this.data.product.logo = this.form.get('logo')?.value;
      if (this.data.product.status !== 'added' && this.data.product.status !== 'deleted') {
        this.data.product.status = RecordStatusEnum.changed;
      }
    }
  }

}


