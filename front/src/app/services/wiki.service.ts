import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';


export enum RecordStatusEnum {
  added = 'added',
  changed = 'changed',
  deleted = 'deleted'
}

export interface IProduct {
  id: string;
  name: string;
  urlname: string;
  logo: string;
  topics: number;
  issues: number;
  addForm?: boolean;
  status?: RecordStatusEnum | undefined;
}

export interface ITopic {
  id: string;
  title: string;
  issues: IProductIssueLight[];
  productId: string;
  status?: RecordStatusEnum | undefined;
}

export interface IProductTopics {
  topics: ITopic[];
}

export interface IProductIssueLight {
  id: string;
  title: string;
  urlTitle?: string;
  status?: RecordStatusEnum | undefined;
}

export interface IProductIssue {
  issueId: string;
  title: string;
  urlTitle?: string;
  html: IParsedQuillHtml[];
  attachments?: any[];
}

export interface IParsedQuillHtml {
  id: string;
  title: string;
  level: number;
  content: string;
}

@Injectable({
  providedIn: 'root'
})
export class WikiService {

  readonly products$: BehaviorSubject<IProduct[]> = new BehaviorSubject<IProduct[]>([]);
  readonly productTopics$: BehaviorSubject<IProductTopics | undefined> = new BehaviorSubject<IProductTopics | undefined>(undefined);
  currentProductUrlTitle$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  currentProductTitle$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  currentProductId$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  issueUrlTitleOrId$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  wikiItemComponentActive$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  topicsEdit$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  issueEdit$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loadingFromServer$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  appInitialized$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  error$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  errorPage$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private loadingTimeout: any;

  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    ) {}

  /** Установка флага loading с задрежкой */
  setLoadingTimeout(): void {
    this.loadingTimeout = window.setTimeout(() => {
      this.loading$.next(true);
    }, 100);
  }

  /** Получение products  */
  async getProductList(firstCall: boolean): Promise<IProduct[]> {
    this.setLoadingTimeout();
    return this.http.get<IProduct[]>(`${environment.backendApi}/api/v1.0.0/wiki/products`).toPromise().then((products) => {
      if (products) {
        this.products$.next(products);
      }
      if (firstCall) {
        this.loading$.next(false);
      }
      window.clearTimeout(this.loadingTimeout);
      return products;
    });
  }

  /** Получние topics  */
  async getProductTopics(productId: string, firstCall: boolean): Promise<IProductTopics> {
    this.setLoadingTimeout();
    this.loadingFromServer$.next(true);
    return this.http.get<IProductTopics>(`${environment.backendApi}/api/v1.0.0/wiki/topics/${productId}`).toPromise().then((productTopics) => {
      if (productTopics) {
        this.productTopics$.next(productTopics);
      }
      if (firstCall) {
        this.loading$.next(false);
      }
      window.clearTimeout(this.loadingTimeout);
      this.loadingFromServer$.next(false);
      return productTopics;
    });
  }

  /** Получение идентификатора issue по его url-name или id */
  async getIssueId(issueUrlTitleOrId: string): Promise<string | undefined> {
    const topics = this.productTopics$.getValue()?.topics;
    if (topics) {
      let currentIssueId: string | undefined;
      for (const topic of topics) {
        const findedIssueByUrlTitle = topic.issues.find(issue => issue.urlTitle === issueUrlTitleOrId);
        const findedIssueById = topic.issues.find(issue => issue.id === issueUrlTitleOrId);
        if (findedIssueByUrlTitle) {
          currentIssueId = findedIssueByUrlTitle.id;
        }
        if (findedIssueById) {
          currentIssueId = findedIssueById.id;
        }
      }
      if (currentIssueId) {
        return currentIssueId;
      } else {
        return undefined;
      }
    }
    return undefined;
  }

  /** Получение issues */
  async getProductIssue(productId: string, issueId: string): Promise<IProductIssue> {
    this.setLoadingTimeout();
    this.loadingFromServer$.next(true);
    return this.http.get<IProductIssue>(`${environment.backendApi}/api/v1.0.0/wiki/issues/${productId}/${issueId}`).toPromise().then((productIssue) => {
      this.loading$.next(false);
      window.clearTimeout(this.loadingTimeout);
      this.loadingFromServer$.next(false);
      return productIssue;
    });
  }

    /** Отправка изменений в products */
    async saveChangedProducts(changedProducts: IProduct[]): Promise<IProduct[] | undefined> {
      this.setLoadingTimeout();
      return this.http.put<IProduct[]>(`${environment.backendApi}/api/v1.0.0/wiki/products/change/`, changedProducts).toPromise().then((products) => {
        if (products) {
          this.products$.next(products);
        }
        this.loading$.next(false);
        window.clearTimeout(this.loadingTimeout);
        return products;
      }).catch((err) => {
        this.loading$.next(false);
        this.openSnackBar(err.error.message);
        window.clearTimeout(this.loadingTimeout);
        return undefined;
      });
    }

  /** Отправка изменений в productTopics */
  async saveChangedProductTopic(changedProductTopic: IProductTopics, productId: string): Promise<IProductTopics | undefined> {
    this.setLoadingTimeout();
    return this.http.put<IProductTopics>(`${environment.backendApi}/api/v1.0.0/wiki/topics/change/${productId}`, changedProductTopic).toPromise().then((productTopics) => {
      if (productTopics) {
        this.productTopics$.next(productTopics);
      }
      this.loading$.next(false);
      window.clearTimeout(this.loadingTimeout);
      return productTopics;
    }).catch((err) => {
      this.openSnackBar(err.error.message);
      this.loading$.next(false);
      window.clearTimeout(this.loadingTimeout);
      return undefined;
    });
  }

  /** Отправка изменений в issues */
  async sendIssueChanges(productId: string, productIssue: IProductIssue): Promise<IProductIssue | undefined> {
    this.setLoadingTimeout();
    return this.http.put<IProductIssue>(`${environment.backendApi}/api/v1.0.0/wiki/issues/change/${productId}/${productIssue.issueId}`, productIssue).toPromise().then((issue) => {
      this.loading$.next(false);
      window.clearTimeout(this.loadingTimeout);
      this.loadingFromServer$.next(false);
      return issue;
    }).catch((err) => {
      this.error$.next(true);
      this.openSnackBar(err.error.message);
      this.loading$.next(false);
      window.clearTimeout(this.loadingTimeout);
      this.loadingFromServer$.next(false);
      return undefined;
    });
  }

    /** Вызов снекбара */
    openSnackBar(message: string): MatSnackBarRef<any> {
      return this._snackBar.open(message, 'ОК', {
        duration: 5000,
        verticalPosition: 'top',
        horizontalPosition: 'center'
      });
    }

}
