import { Pipe, PipeTransform } from '@angular/core';
// Local
import { IProduct } from './wiki.service';

@Pipe({
  name: 'searchProduct'
})
export class SearchProductPipe implements PipeTransform {

  transform(products: IProduct[], search = ''): IProduct[] {
    if (!search.trim()) {
      return products;
    }

    return products.filter(product => {
      return product.name.toLowerCase().includes(search.toLowerCase());
    });
  }
}
