import { Pipe, PipeTransform } from '@angular/core';
// Local
import { IProductIssueLight } from './wiki.service';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(issues: IProductIssueLight[], search = ''): IProductIssueLight[] {
    if (!search.trim()) {
      return issues;
    }

    return issues.filter(issue => {
      return issue.title.toLowerCase().includes(search.toLowerCase());
    });
  }
}
