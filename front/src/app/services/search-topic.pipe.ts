import { Pipe, PipeTransform } from '@angular/core';
// Local
import { ITopic } from './wiki.service';

@Pipe({
  name: 'searchTopic'
})
export class SearchTopicPipe implements PipeTransform {

  transform(topics: ITopic[], search = ''): ITopic[] {
    if (!search.trim()) {
      return topics;
    }

    return topics.filter(topic => {
      return topic.issues.filter(issue => {
        return issue.title.toLowerCase().includes(search.toLowerCase());
      }).length;
    });

  }

}

