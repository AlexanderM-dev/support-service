import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
// local
import { IProductTopics, IProductIssueLight, WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss'],
})
export class TopicsComponent {

  productTopics$: BehaviorSubject<IProductTopics | undefined>;
  currentProductUrlTitle$: BehaviorSubject<string>;
  issueUrlTitleOrId$: BehaviorSubject<string>;
  loadingFromServer$: BehaviorSubject<boolean>;

  constructor(
    private service: WikiService,
    private router: Router
  ) {
    this.productTopics$ = this.service.productTopics$;
    this.currentProductUrlTitle$ = this.service.currentProductUrlTitle$;
    this.issueUrlTitleOrId$ = this.service.issueUrlTitleOrId$;
    this.loadingFromServer$ = this.service.loadingFromServer$;
  }

  /** Переход в Issue по url-title (если есть) или по идентификатору Issue */
  openIssue(issue: IProductIssueLight): void {
    if (!this.loadingFromServer$.value) {
      if (issue.urlTitle && this.currentProductUrlTitle$.value) {
        this.router.navigate([`wiki/${this.currentProductUrlTitle$.value}/${issue.urlTitle}`]);
      } else {
        this.router.navigate([`wiki/${this.currentProductUrlTitle$.value}/${issue.id}`]);
      }
    }
  }

}
