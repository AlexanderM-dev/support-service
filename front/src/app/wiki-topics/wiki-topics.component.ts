import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
// Local
import { IProductTopics, IProductIssueLight, ITopic, RecordStatusEnum, WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-wiki-topics',
  templateUrl: './wiki-topics.component.html',
  styleUrls: ['./wiki-topics.component.scss']
})
export class WikiTopicsComponent implements OnInit, OnDestroy {

  RecordStatusEnum = RecordStatusEnum;

  loading$: BehaviorSubject<boolean>;
  currentProductTitle$: BehaviorSubject<string>;
  productTopicsShot?: string;
  productTopics?: IProductTopics;
  edit = false;
  productId?: string;
  disabled = false;
  productTopicsShotOnDelete?: string;
  currentIssueId = '';
  currentTopicId = '';
  currentProductId: string | undefined;
  searchString = '';
  issuePanelOpenState?: ITopic;
  newIssueName = '';

  topicPanelOpenState = false;
  newTopicName = '';

  issueErrors: Record<string, boolean> = {};

  routeParams?: Subscription;

  constructor(
    private service: WikiService,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    this.loading$ = this.service.loading$;
    this.currentProductTitle$ = this.service.currentProductTitle$;
  }

  ngOnInit(): void {
    this.routeParams = this.route.params.subscribe(async (params: Params) => {
      try {
        if (!this.service.appInitialized$.value) {
          await this.service.getProductList(true);
          this.service.appInitialized$.next(true);
        }
        const currentProductTitle = this.service.products$.value.find(item => item.urlname === params.productName)?.name;
        if (currentProductTitle) {
          this.service.currentProductTitle$.next(currentProductTitle);
        }
        this.currentProductId = this.service.products$.value.find(item => item.urlname === params.productName)?.id;
        if (this.currentProductId) {
          this.service.currentProductId$.next(this.currentProductId);
          this.productId = this.service.currentProductId$.value;
          if (this.productId) {
            await this.getProductTopics(this.productId);
          }
        } else {
          this.router.navigate(['/error']);
        }
      } catch (error) {
        console.error(error);
      }
    });
  }

  ngOnDestroy(): void {
    this.service.topicsEdit$.next(false);
    if (this.routeParams) {
      this.routeParams.unsubscribe();
    }
    this._snackBar.dismiss();
  }

  /** Вызов снекбара */
  openSnackBar(message: string): MatSnackBarRef<any> {
    return this._snackBar.open(message, 'Cancel', {
      duration: 5000,
    });
  }

  /** Получение Topics по идентификатору продукта */
  async getProductTopics(productId: string): Promise<void> {
    try {
      this.productTopics = await this.service.getProductTopics(productId, true);
    } catch (error) {
      console.error(error);
    }
  }

  /** Тогл редактированием Topics */
  toggleEditItem(): void {
    if (!this.edit) {
      this.productTopicsShot = JSON.stringify(this.productTopics);
      this.edit = true;
      this.service.topicsEdit$.next(true);
    } else {
      if (this.productTopicsShot) {
        this.productTopics = JSON.parse(this.productTopicsShot);
        this.edit = false;
        this.service.topicsEdit$.next(false);
      }
    }
    this.newIssueName = '';
    this.newTopicName = '';
    this.topicPanelOpenState = false;
    this.issuePanelOpenState = undefined;
    this._snackBar.dismiss();
  }

  /** Функция CDK drag and drop для Topics */
  dropTopics(event: CdkDragDrop<string[]>): void {
    if (this.productTopics) {
      moveItemInArray(this.productTopics.topics, event.previousIndex, event.currentIndex);
      for (const topic of this.productTopics.topics) {
        if (topic.status !== 'added' && topic.status !== 'deleted') {
          topic.status = RecordStatusEnum.changed;
        }
      }
    }
  }

  /** Функция CDK drag and drop для Issues с отметкой об изменнении Topic */
  dropIssues(event: CdkDragDrop<string[]>, topic: ITopic): void {
    if (topic.issues) {
      moveItemInArray(topic.issues, event.previousIndex, event.currentIndex);
      if (topic.status !== 'added') {
        topic.status = RecordStatusEnum.changed;
      }
      for (const issue of topic.issues) {
        if (issue.status !== 'added' && issue.status !== 'deleted') {
          issue.status = RecordStatusEnum.changed;
        }
      }
    }
  }

  /** Удаление Topic - добавление метки удаленного Topic */
  deleteTopic(requestTopic: ITopic): void {
    this.productTopicsShotOnDelete = JSON.stringify(this.productTopics);
    if (requestTopic.status !== RecordStatusEnum.added) {
      requestTopic.status = RecordStatusEnum.deleted;
    }

    if (requestTopic.status === RecordStatusEnum.added) {
      if (this.productTopics) {
        const index = this.productTopics.topics.findIndex(topic => topic === requestTopic);
        if (index === 0 || index) {
          this.productTopics.topics.splice(index, 1);
        }
      }
    }
    const snack = this.openSnackBar(`Topic deleted`);
    snack.onAction().subscribe(() => {
      if (this.productTopicsShotOnDelete) {
        this.productTopics = JSON.parse(this.productTopicsShotOnDelete);
      }
    });

  }

  /** Удаление Issue - добавление метки удаленного Issue + метки измененного Topic */
  deleteIssue(requestTopic: ITopic, requestIssue: IProductIssueLight): void {
    this.productTopicsShotOnDelete = JSON.stringify(this.productTopics);
    if (requestIssue.status !== RecordStatusEnum.added) {
      requestIssue.status = RecordStatusEnum.deleted;
      requestTopic.status = RecordStatusEnum.changed;
      if (this.issueErrors[requestIssue.id]) {
        delete this.issueErrors[requestIssue.id];
      }
    }
    if (requestIssue.status === RecordStatusEnum.added) {
      if (this.productTopics) {
        const currentTopic = this.productTopics.topics.find(topic => topic === requestTopic);
        if (currentTopic) {
          const currentIssueIndex = currentTopic.issues.findIndex(issue => issue === requestIssue);
          if (currentIssueIndex || currentIssueIndex === 0) {
            currentTopic.issues.splice(currentIssueIndex, 1);
            if (this.issueErrors[requestIssue.id]) {
              delete this.issueErrors[requestIssue.id];
            }
          }
        }
      }
    }
    const snack = this.openSnackBar(`Issue deleted`);
    snack.onAction().subscribe(() => {
      if (this.productTopicsShotOnDelete) {
        this.productTopics = JSON.parse(this.productTopicsShotOnDelete);
      }
    });
  }

  /** Тогл инпута нового Topic */
  toggleNewTopicInput(): void {
    this.topicPanelOpenState = !this.topicPanelOpenState;
  }

  /** Добавление нового Topic */
  addNewTopic(): void {
    if (this.productTopics && this.currentProductId) {
      const newTopic = {
        id: uuidv4(),
        title: this.newTopicName,
        productId: this.currentProductId,
        issues: [],
        status: RecordStatusEnum.added
      };
      this.productTopics.topics.push(newTopic);
    }
    this.newTopicName = '';
  }

  /** Тогл инпута нового Issue (Material expansion panel) */
  toggleIssue(topic: ITopic): void {
    if (this.issuePanelOpenState === topic) {
      this.issuePanelOpenState = undefined;
      this.newIssueName = '';
    } else {
      this.issuePanelOpenState = topic;
      this.newIssueName = '';
    }
  }

  /** Добавление нового Issue */
  addNewIssue(topic: ITopic): void {
    topic.issues.push({
      id: uuidv4(),
      title: this.newIssueName,
      urlTitle: '',
      status: RecordStatusEnum.added
    });
    if (topic.status !== 'added') {
      topic.status = RecordStatusEnum.changed;
    }
    this.newIssueName = '';
  }

  /** Сохранение изменения редактирования (отправка на сервер изменений и получение в ответ измененного productTopics) */
  async save(): Promise<void> {
    if (!this.issueErrorsCount() && this.productTopics) {
      let topicsHasChanges = false;
      for (const topic of this.productTopics.topics) {
        if (topic.status) {
          topicsHasChanges = true;
        }
      }
      if (topicsHasChanges) {
        const topics = await this.service.saveChangedProductTopic(this.productTopics, this.service.currentProductId$.value);
        if (topics) {
          this.productTopics = topics;
        }
      }
      this.edit = false;
      this.service.topicsEdit$.next(false);
      this._snackBar.dismiss();
    }
  }

  /** Проверка наличия ошибок */
  determineError(issue: IProductIssueLight): boolean {
    const result: boolean = !issue.title.replace(/ /g, '').length;
    if (result) {
      this.issueErrors[issue.id] = true;
    } else {
      if (this.issueErrors[issue.id]) {
        delete this.issueErrors[issue.id];
      }
    }
    return result;
  }

  /** Отслеживание кол-ва ошибок */
  issueErrorsCount(): number {
    return Object.keys(this.issueErrors).length;
  }

  /** Отслеживание focus-in в конкретном инпуте */
  focusInInputTopic(topicId: string): void {
    this.currentTopicId = topicId;
  }

  /** Отслеживание focus-in в конкретном инпуте */
  focusInInput(issueId: string): void {
    this.currentIssueId = issueId;
  }

  /** Отслеживание focus-out в конкретном инпуте */
  focusOutInput(): void {
    this.currentIssueId = '';
    this.currentTopicId = '';
  }

  /** Переход в Issue по url-title (если есть) или по идентификатору Issue */
  openIssue(issue: IProductIssueLight): void {
    if (issue.urlTitle) {
      this.router.navigate([issue.urlTitle], { relativeTo: this.route });
    } else {
      this.router.navigate([issue.id], { relativeTo: this.route });
    }
  }

  /** Добавление статуса changed в Topic при изменении его имени */
  changeTopicStatus(topic: ITopic): void {
    if (topic.status !== 'added' && topic.status !== 'deleted') {
      topic.status = RecordStatusEnum.changed;
    }
  }

  /** Добавление статуса changed в Topic и Issue при изменении имени Issue */
  changeIssueStatus(topic: ITopic, issue: IProductIssueLight): void {
    if (topic.status !== 'added' && topic.status !== 'deleted') {
      topic.status = RecordStatusEnum.changed;
      if (issue.status !== 'added' && issue.status !== 'deleted') {
        issue.status = RecordStatusEnum.changed;
      }
    }
  }

}
