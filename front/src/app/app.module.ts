// Angular
import { NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Angular Material
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
// Angular CDK
import { DragDropModule } from '@angular/cdk/drag-drop';
// Quill-editor
import { QuillModule } from 'ngx-quill';
// Local
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DialogContentComponent, WikiProductsComponent } from './wiki-products/wiki-products.component';
import { WikiTopicsComponent } from './wiki-topics/wiki-topics.component';
import { WikiItemComponent } from './wiki-item/wiki-item.component';
import { SearchPipe } from './services/search.pipe';
import { SearchTopicPipe } from './services/search-topic.pipe';
import { SearchProductPipe } from './services/search-product.pipe';
import { WrapperComponent } from './wrapper/wrapper.component';
import { NavigationComponent } from './navigation/navigation.component';
import { TopicsComponent } from './topics/topics.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    WikiProductsComponent,
    WikiTopicsComponent,
    WikiItemComponent,
    SearchPipe,
    SearchTopicPipe,
    SearchProductPipe,
    WrapperComponent,
    NavigationComponent,
    TopicsComponent,
    DialogContentComponent,
    ErrorPageComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatBadgeModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatCardModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    DragDropModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatDialogModule,
    MatListModule,
    QuillModule.forRoot({
      modules: {
        syntax: true
      }
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
