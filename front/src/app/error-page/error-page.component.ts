import { Component, OnDestroy, OnInit } from '@angular/core';
import { WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit, OnDestroy {

  constructor(
    private service: WikiService
  ) { }

  ngOnInit(): void {
    this.service.errorPage$.next(true);
  }
  ngOnDestroy(): void {
    this.service.errorPage$.next(false);
  }

}
