import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
// Local
import { WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
})
export class WrapperComponent {

  wikiItemComponentActive$: BehaviorSubject<boolean>;

  constructor(
    private service: WikiService
  ) {
    this.wikiItemComponentActive$ = this.service.wikiItemComponentActive$;
  }

}
