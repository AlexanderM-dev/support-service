import { QuillModules } from 'ngx-quill';

export const config: QuillModules = {
  syntax: true,
  toolbar: [
    [{ header: 1 }, { header: 2 }],               // custom button values
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ script: 'sub' }, { script: 'super' }],      // superscript/subscript
    [{ size: ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ color: [] }, { background: [] }],          // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],
    ['clean'],
    ['link', 'image', 'video']
  ],
};
