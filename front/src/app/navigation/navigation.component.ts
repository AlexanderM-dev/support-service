import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
// Local
import { WikiService } from '../services/wiki.service';

interface INavButtons {
  name: string;
  urlName: string;
  materialIconName: string;
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {

  activatedRoute = '';
  routerEvents?: Subscription;

  topicsEdit$: BehaviorSubject<boolean>;
  issueEdit$: BehaviorSubject<boolean>;
  errorPage$: BehaviorSubject<boolean>;

  navButtons: INavButtons[] = [
    {
      name: 'Wiki',
      urlName: 'wiki',
      materialIconName: 'assignment'
    },
    {
      name: 'About',
      urlName: 'about',
      materialIconName: 'help_center'
    },
  ];

  constructor(
    private router: Router,
    private service: WikiService,
  ) {
    this.topicsEdit$ = this.service.topicsEdit$;
    this.issueEdit$ = this.service.issueEdit$;
    this.errorPage$ = this.service.errorPage$;
  }

  ngOnInit(): void {
    this.routerEvents = this.router.events.subscribe(() => {
      this.activatedRoute = this.router.routerState.snapshot.url.split('/')[1];
    });
  }

  ngOnDestroy(): void {
    if (this.routerEvents) {
      this.routerEvents.unsubscribe();
    }
  }

}
