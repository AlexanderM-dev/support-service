import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
// Local
import { config } from '../common';
import { IParsedQuillHtml, IProductIssue, IProductTopics, WikiService } from '../services/wiki.service';

@Component({
  selector: 'app-wiki-item',
  templateUrl: './wiki-item.component.html',
  styleUrls: ['./wiki-item.component.scss']
})
export class WikiItemComponent implements OnInit, OnDestroy {

  loading$: BehaviorSubject<boolean>;
  error$: BehaviorSubject<boolean>;
  productTopics$: BehaviorSubject<IProductTopics | undefined>;
  currentProductTitle$: BehaviorSubject<string>;
  currentProductUrlTitle$: BehaviorSubject<string>;
  productIssue?: IProductIssue;
  edit = false;
  quillForm: FormGroup;
  config = config;
  productId?: string;
  currentTextBlockId = '';
  htmlBlockHasContent = false;
  allowedSymbols = 'abcdefghijklmnopqrstuvwxyz0123456789-_';
  routeParams?: Subscription;

  quillBlocks: IParsedQuillHtml[] = [];

  titleControl = new FormControl(null, [
    Validators.required,
    Validators.pattern(/^[^ ]+(.+)?$/)
  ]);

  urlTitleControl?: FormControl;

  @ViewChild('scrollbox') scrollContentBox: ElementRef | undefined;
  @ViewChild('scrollboxinside') scrollContentBoxElement: ElementRef | undefined;
  @ViewChild('.ql-editor') quillEd: ElementRef | undefined;

  constructor(
    private service: WikiService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.loading$ = service.loading$;
    this.quillForm = new FormGroup({
      content: new FormControl(null)
    });
    this.currentProductTitle$ = this.service.currentProductTitle$;
    this.currentProductUrlTitle$ = this.service.currentProductUrlTitle$;
    this.productTopics$ = this.service.productTopics$;
    this.error$ = this.service.error$;
  }

  /** Валидатор уникальности url-name */
  static uniqueUrlName(productTopics: IProductTopics | undefined, productIssue: IProductIssue | undefined): ValidatorFn {
    return (control: AbstractControl) => {
      if (productTopics && productIssue) {
        for (const issues of productTopics.topics) {
          for (const issue of issues.issues) {
            if (issue.urlTitle === control.value && issue.id !== productIssue.issueId && control.value !== null) {
              return {
                notUnique: true
              };
            }
          }
        }
      }
      return null;
    };
  }

  ngOnInit(): void {
    this.routeParams = this.route.params.subscribe(async (params: Params) => {
      this.service.currentProductUrlTitle$.next(params.productName);
      this.service.issueUrlTitleOrId$.next(params.urlTitleOrId);
      this.edit = false;
      try {
        if (!this.service.appInitialized$.value) {
          await this.service.getProductList(true);
          this.service.appInitialized$.next(true);
        }
        const currentProductTitle = this.service.products$.value.find(item => item.urlname === params.productName)?.name;
        if (currentProductTitle) {
          this.service.currentProductTitle$.next(currentProductTitle);
        }
        const currentProductId = this.service.products$.value.find(item => item.urlname === params.productName)?.id;
        if (currentProductId) {
          this.service.currentProductId$.next(currentProductId);
        } else {
          this.router.navigate(['/error']);
          return;
        }
        this.productId = this.service.currentProductId$.value;
        if (this.productId) {
          await this.getProductTopics(this.productId);
          this.service.wikiItemComponentActive$.next(true);
          const issueId = await this.service.getIssueId(params.urlTitleOrId);
          if (issueId) {
            await this.getProductIssue(this.productId, issueId);
            if (this.productIssue) {
              this.urlTitleControl = new FormControl(null, [
                WikiItemComponent.uniqueUrlName(this.productTopics$.value, this.productIssue)
              ]);
              if (this.productIssue.html) {
                this.quillBlocks = this.productIssue.html;
                if (this.stripHtml(this.quillBlocks[0].content)) {
                  this.htmlBlockHasContent = true;
                } else {
                  this.htmlBlockHasContent = false;
                }
              } else {
                this.quillBlocks = [];
                this.htmlBlockHasContent = false;
              }
              this.titleControl.setValue(this.productIssue.title);
              this.urlTitleControl.setValue(this.productIssue.urlTitle);
              setTimeout(() => {
                if (this.scrollContentBox) {
                  this.scrollContentBox.nativeElement.scrollTop = 0;
                }
                this.onScroll();
              }, 0);
            }
          } else {
            this.router.navigate(['/error']);
          }
        }
      } catch (error) {
        console.error(error);
      }
    });
  }

  ngOnDestroy(): void {
    this.service.wikiItemComponentActive$.next(false);
    this.service.issueEdit$.next(false);
    if (this.routeParams) {
      this.routeParams.unsubscribe();
    }
  }

  /** Получение Topics по идентификатору продукта */
  async getProductTopics(productId: string): Promise<void> {
    try {
      await this.service.getProductTopics(productId, false);
    } catch (error) {
      console.error(error);
    }
  }

  /** Получение Issues по идентификатору продукта и по идентификатору Issue */
  async getProductIssue(productId: string, issueId: string): Promise<void> {
    try {
      this.productIssue = await this.service.getProductIssue(productId, issueId);
    } catch (error) {
      console.error(error);
    }
  }

  /** Тогл редактированием Issue */
  editItem(): void {
    this.edit = !this.edit;
    this.service.issueEdit$.next(this.edit);
    if (this.edit && this.urlTitleControl) {
      if (this.productIssue) {
        this.titleControl.setValue(this.productIssue.title);
        this.urlTitleControl.setValue(this.productIssue.urlTitle);
        if (!this.htmlBlockHasContent) {
          this.quillForm.controls.content.setValue('\n');
        } else {
          this.quillForm.controls.content.setValue(
            // заменяем лишние p-br-p, которые образуются в Quill-editor при парсе блоков в html-строку
            this.htmlBlocksToString(this.quillBlocks)
              .replace(/<p><br><\/p>|<p class="ql-align-center"><br><\/p>|<p class="ql-align-justify"><br><\/p>|<p class="ql-align-right"><br><\/p>/g, '<br>')
              .replace(/<p>|<p class="ql-align-center">|<p class="ql-align-justify">|<p class="ql-align-right">/g, '')
              .replace(/<\/p>/g, '<br>')
          );
        }
      }
    } else {
      if (this.quillBlocks.length) {
        this.currentTextBlockId = this.quillBlocks[0].id;
      }
    }
  }

  /** Сохранение изменения редактирования (отправка на сервер изменений и получение в ответ измененного productIssue) */
  async save(): Promise<void> {
    if (this.productId && this.productIssue && this.urlTitleControl) {
      this.parseQuillHtmlContent();
      this.productIssue.title = this.titleControl.value;
      this.productIssue.urlTitle = this.urlTitleControl.value;
      this.productIssue.html = this.quillBlocks;
      try {
        const timeOut = window.setInterval(() => {
          this.service.loading$.next(true);
        }, 150);
        const issue = await this.service.sendIssueChanges(this.productId, this.productIssue);
        if (issue) {
          this.productIssue = issue;
        }
        await this.getProductTopics(this.productId);
        window.clearInterval(timeOut);
        this.service.loading$.next(false);
        this.onScroll();
        if (this.productIssue && !this.error$.value) {
          if (this.productIssue.urlTitle && this.currentProductUrlTitle$.value) {
            this.router.navigate([`wiki/${this.currentProductUrlTitle$.value}/${this.productIssue.urlTitle}`]);
          } else {
            this.router.navigate([`wiki/${this.currentProductUrlTitle$.value}/${this.productIssue.issueId}`]);
          }
        }
        if (this.error$.value) {
          this.service.error$.next(false);
        }
      } catch (error) {
        this.service.loading$.next(false);
        console.error(error);
      }
      this.edit = false;
      this.service.issueEdit$.next(false);
    }
  }

  /** Приведение к валидному url-name */
  toUrlValidName(event: string): void {
    if (this.productIssue && this.urlTitleControl) {
      const str = event.replace(/ /g, '-').toLowerCase();
      let result = '';
      for (const symb of str) {
        if (this.allowedSymbols.includes(symb)) {
          result = result + symb;
        }
      }
      if (result !== this.urlTitleControl.value) {
        this.urlTitleControl.setValue(result);
      }
    }
  }

  /** Скролл к нужному блоку дом дерева */
  scrollToTitle(blockId: string): void {
    document.getElementById(blockId)?.scrollIntoView();
  }

  /** Получение текста из HTML-строки */
  stripHtml(html: string): string {
    const tmp: HTMLElement = document.createElement('DIV');
    tmp.innerHTML = html;
    const result = tmp.textContent || tmp.innerText || '';
    // проверка на BOM (символ 65279)
    if (result.length === 1 && result.charCodeAt(0) === 65279) {
      return '';
    }
    return result;
  }

  /** Отслеживание текущей позиции скролла для корректного отображение в правой панели навигации */
  onScroll(): void {
    if (this.scrollContentBox && this.scrollContentBoxElement) {
      const scrollPosition: number = this.scrollContentBox.nativeElement.scrollTop;
      const childrens: HTMLElement[] = this.scrollContentBoxElement.nativeElement.children;
      let currentPosition = 0;
      if (childrens.length > 0) {
        this.currentTextBlockId = childrens[0].id;
      }
      for (const children of childrens) {
        const childrenHeight = children.clientHeight;
        if (currentPosition < scrollPosition) {
          currentPosition = currentPosition + childrenHeight;
          if (children.id) {
            this.currentTextBlockId = children.id;
          }
        }
      }
    }
  }

  /** Парс HTML-строки на quillBlocks */
  parseQuillHtmlContent(): void {
    const elmts: HTMLCollection = document.getElementsByClassName('ql-editor');
    const result: IParsedQuillHtml[] = [];
    let title = '';
    let content = '';
    let level = 1;
    for (const elmt of Array.from(elmts[0].children)) {
      if (elmt.localName === 'h1' || elmt.localName === 'h2' || elmt.localName === 'h3' || elmt.localName === 'h4' || elmt.localName === 'h5') {
        if (content) {
          result.push({
            id: uuidv4(),
            title: this.stripHtml(title),
            content,
            level
          });
        }
        title = elmt.outerHTML;
        content = title;
        level = +elmt.localName[1];
        continue;
      } else {
        content = content + elmt.outerHTML;
        continue;
      }
    }
    result.push({
      id: uuidv4(),
      title: this.stripHtml(title),
      content,
      level
    });
    this.quillBlocks = result;

    if (this.stripHtml(this.quillBlocks[0].content)) {
      this.htmlBlockHasContent = true;
    } else {
      this.htmlBlockHasContent = false;
    }
    this.currentTextBlockId = this.quillBlocks[0].id;
  }

  /** Приведение quillBlocks к HTML-строке */
  htmlBlocksToString(htmlBlocks: IParsedQuillHtml[]): string {
    let htmlString = '';
    for (const htmlBlock of htmlBlocks) {
      htmlString = htmlString + htmlBlock.content;
    }
    return htmlString;
  }

}
