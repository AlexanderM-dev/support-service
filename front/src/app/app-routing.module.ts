import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Local
import { WikiItemComponent } from './wiki-item/wiki-item.component';
import { WikiProductsComponent } from './wiki-products/wiki-products.component';
import { WikiTopicsComponent } from './wiki-topics/wiki-topics.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AboutComponent } from './about/about.component';

let routes: Routes = [];

routes = [
  { path: 'about', component: AboutComponent },
  { path: 'wiki', component: WikiProductsComponent },
  { path: '', redirectTo: '/wiki', pathMatch: 'full'},
  { path: 'wiki/:productName', component: WikiTopicsComponent },
  { path: 'wiki/:productName/:urlTitleOrId', component: WikiItemComponent },
  { path: 'error', component: ErrorPageComponent },
  { path: '**', redirectTo: '/error' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
