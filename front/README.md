# Wiki Constructor Front-end app

Wiki Constructor Front-end app - is a Front-end part of [Wiki Constructor](https://gitlab.com/AlexanderM-dev/support-service)

[Back-end](https://gitlab.com/AlexanderM-dev/support-service/-/tree/master/back) part

## Install

To run this app you need:

Run in console `npm i`

## Run

Run in console `npm start`